import '../App.css';
import {React, useEffect} from 'react';
import { useRecoilState } from "recoil";
import axios from 'axios';
import { todosState } from '../atoms/todosState.js'
import { todoState } from '../atoms/todoState.js'

import '../index.css';

function Form() {

    const [todo, setTodo] = useRecoilState(todoState)

    const [todos, setTodos] = useRecoilState(todosState)

    let addTodo=()=>{
        console.log("this is todo from ADD TO DO"+todo)
      axios.post(`http://localhost:3016/todos/add`,({todo:todo.todo, done:todo.done}))
      .then((res)=>{
          console.log(res.data)
      })
      .catch((err)=>{
        console.log(err)
      })
    }

    const handleInput = e => {
        setTodo({todo: e.target.value, done: false})
        console.log("this is todo from handle Input -> "+todo.todo)
    }

    // useEffect(() => {
    //     addTodo()
    //   }, []);

    return (
        <div>
            <form onSubmit={addTodo}>
                <div className="searchbox-wrap">
                    <input className="inputField" onChange={handleInput}></input>
                    <button className="submit">SUBMIT</button>
                </div>
            </form>
        </div>
    )

}

export default Form;